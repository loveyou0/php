<?php
/*********************************************
* 文件：uploadimg.php
* 用途：图片上传程序
*********************************************/
// 图片目录
img_dir = "../upload/";
// …… html 显示上传界面
/* 图片上传处理 */
// 把图片传到服务器
// 初始化变量
uploaded = 0;
unuploaded = 0;
//只允许五张图片上传
for (i=0; i<=5; i++)
{
//获取当前图片的信息
is_file = _FILES['imgfile']['name'][i];
//如果当前图片不为空
if (!empty(is_file))
{
//把当前图片的信息存储到变量里
result[i] = "
<tr class=td2 align=center>
<td>". _FILES['imgfile']['name'][i] ."</td>
<td>". round(_FILES['imgfile']['size'][i]/1024, 2) ."K</td>
<td>". _FILES['imgfile']['type'][i] ."</td>
<td>";
// 判断上传的图片的类型是不是jpg,gif,png,bmp中的一种，同时判断是否上传成功
if (
_FILES['imgfile']['type'][i] == "image/pjpeg" ||
_FILES['imgfile']['type'][i] == "image/gif" ||
_FILES['imgfile']['type'][i] == "image/x-png" ||
_FILES['imgfile']['type'][i] == "image/bmp"
)
{
//如果上传的文件没有在服务器上存在
if (!file_exists(img_dir . _FILES['imgfile']['name'][i]))
{
//把图片文件从临时文件夹中转移到我们指定上传的目录中
move_uploaded_file(_FILES['imgfile']['tmp_name'][i], 
img_dir . _FILES['imgfile']['name'][i]);
result[i] .= "成功";
uploaded++;
}
else //如果文件已经在服务器上存在
{
result[i] .= "<font color=red>文件已存在</font>";
unuploaded++;
continue;
}
}
else
{
result[i] .= "<font color=red>失败</font>";
unuploaded++;
}
result[i] .= "</td></tr>";
} //end if
} // end for
// 如果没有选择任何图片
if (empty(result))
{
prompt_msg("错误信息", "没有选择任何图片。", "返回上一步", "uploadimg.php?action=upload" );
exit();
}
// 显示所有上传后的结果
echo " <table cellpadding=4 cellspacing=1 border=0 class=table width=400 align=left>
<tr class=navi align=center>
<td>文件名</td>
<td>大小</td>
<td>类型</td>
<td>上传结果</td>
</tr>
";
foreach( result as value)
{
echo value;
}
echo "<tr class=td1>
<td colspan=4>共上传 " . (uploaded + unuploaded) . ", 成功: uploaded, 失败:<font color=red> unuploaded</font> </td>
</tr>
<tr class=navi>
<td colspan=4 align=center>[ <a href='uploadimg.php?action=upload' title='继续上传'>继续上传</a> ] &nbsp;&nbsp;&nbsp;&nbsp;[ <a href='image.php' title='浏览图片'>浏览图片</a> ]</td>
</tr>
</table> 
";
?>